<?php
/**
 * @file Mail-In-One rules file.
 */

/**
 * Implements hook_rules_action_info().
 */
function mailinone_rules_rules_action_info() {
  $actions = array();

  $actions['mailinone_rules_add_user'] = array(
    'label' => t('Add user to newsletter'),
    'group' => 'Mail-In-One',
    'parameter' => array(
      'email' => array(
        'label' => t('E-mail address'),
        'type' => 'text',
        'default mode' => 'selector',
      ),
      'firstname' => array(
        'label' => t('First name'),
        'type' => 'text',
        'optional' => TRUE,
        'default mode' => 'selector',
      ),
      'lastname' => array(
        'label' => t('Last name'),
        'type' => 'text',
        'optional' => TRUE,
        'default mode' => 'selector',
      ),
    ),
    'named parameter' => TRUE,
  );

  return $actions;
}
