<?php
/**
 * @file Settings form file.
 */

/**
 * Admin settings form
 */
function mailinone_settings_form($form, &$form_state) {
  $config = mailinone_get_config();

  $salutation = variable_get('mailinone_salutation');
  $form['mailinone_salutation'] = array(
    '#type' => 'textarea',
    '#title' => t('Salutation'),
    '#default_value' => $salutation,
    '#description' => t('The possible values this field can contain. Enter one value per line, in the format key|label.'),
  );

  $form['mailinone'] = array(
    '#tree' => TRUE,
  );

  $form['mailinone']['API_KEY'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#description' => t('Please enter your maileon API key here.'),
    '#default_value' => $config['API_KEY'],
  );

  $form['mailinone']['DOI_CODE'] = array(
    '#type' => 'textfield',
    '#title' => t('DOI Code'),
    '#description' => t('Please enter your Double-Opt-In code here.'),
    '#default_value' => $config['DOI_CODE'],
  );

  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
    'mailinone' => array(
      '#tree' => TRUE,
    ),
  );

  $form['advanced']['mailinone']['DEBUG'] = array(
    '#type' => 'checkbox',
    '#title' => t('Debug'),
    '#description' => t('Enable to activate the debug mode. Never do this on a production system!'),
    '#return_value' => 1,
    '#default_value' => $config['DEBUG'],
  );

  $form['advanced']['mailinone']['TIMEOUT'] = array(
    '#type' => 'numberfield',
    '#title' => t('Timeout (seconds)'),
    '#description' => t('How long should the server wait for API response from maileon?'),
    '#min' => 30,
    '#default_value' => $config['TIMEOUT'],
  );

  $form['advanced']['mailinone']['BASE_URI'] = array(
    '#type' => 'textfield',
    '#title' => t('Base URI'),
    '#description' => t('If required you can edit the base uri here.'),
    '#default_value' => $config['BASE_URI'],
  );

  $form['advanced']['mailinone']['THROW_EXCEPTION'] = array(
    '#type' => 'checkbox',
    '#title' => t('Throw exceptions'),
    '#description' => t('Enable if you want the maileon API to throw exceptions.'),
    '#return_value' => 1,
    '#default_value' => $config['THROW_EXCEPTION'],
  );

  return system_settings_form($form);
}
