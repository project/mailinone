INTRODUCTION
------------

The module allows to integrate a newsletter registration form with Mail-In-One. 
The module sends the form data to Mail-In-One and Mail-In-One triggers automatically 
a Double-Opt-In email or a marketing automation flow.

 * For a full description of the module, visit the project page:
   https://drupal.org/sandbox/gurksor/2980741

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2980741


REQUIREMENTS
------------

This module requires the following modules:

 * Libraries API (https://drupal.org/project/libraries)

This module requires the following 3rd-party libraries:
 * Maileon PHP Client (https://www.maileon.de/integrationen)

INSTALLATION
------------
 
 * Download the Maileon PHP Client and place it in your libraries folder.
   The MaileonApiClient.php should be available under libraries\maileon\client
 * Install as you would normally install a contributed Drupal module.
   Visit: https://drupal.org/documentation/install/modules-themes/modules-7 for further information.



CONFIGURATION
-------------

 * Insert the API and DOI key in Administration » Configuration » Services » Mail-In-One
 * Add the "Mail-in-One Signup" block to your page



MAINTAINERS
-----------

Current maintainers:
 * Alexander Gura (gurksor) - https://drupal.org/user/3269280

This project has been developed by:
 * Fewclicks GmbH
   Visit https://fewclicks.io for more information.

Supporting organisation:
 * treaction AG
   Visit http://treaction.de/en for more information
